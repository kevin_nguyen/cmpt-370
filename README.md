Start Python http.server and open http://localhost:8000/main.html to view.

Alternatively run StartDemo.bat to do the same thing (requires Python 3.x or above)

**Controls:**
- W: rotate up
- A: rotate left
- S: rotate down
- D: rotate right
- Q: roll counter-clockwise
- E: roll clockwise


Based on early implementations of Zach's WebGL engine, modified to support own needs.